//
//  MainViewController.swift
//  GetResults
//
//  Created by Alex Balan on 27/11/2018.
//  Copyright © 2018 Alex Balan. All rights reserved.
//

import UIKit
import Firebase
import Speech
import AVFoundation
import SQLite3

class WhyViewController: UIViewController {

    @IBOutlet weak var b1: UIButton!
    @IBOutlet weak var b2: UIButton!
    @IBOutlet weak var b3: UIButton!
    @IBOutlet weak var b4: UIButton!
    @IBOutlet weak var b5: UIButton!
    @IBOutlet weak var tsLbl: UILabel!

    @IBOutlet weak var recordView: UIView!
    @IBOutlet weak var recordTable: UITableView!

    @IBOutlet weak var recordButton: UIButton!

    var goalID:Int!
    var currentGoal:Int!

    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!

    let audioEngine = AVAudioEngine()

    let speechRecognizer: SFSpeechRecognizer? = SFSpeechRecognizer()

    let request = SFSpeechAudioBufferRecognitionRequest()
    var recognitionTask: SFSpeechRecognitionTask?

    var audioFilename:URL!
    var audioName:String!

    var db: OpaquePointer?

    var txtLblString:String?

    var isRecording = false

    var RV:RecordView!

    var recordList = [RecordList]()


    override func viewDidLoad() {
        super.viewDidLoad()

        resetButtons()
        b1.setTitleColor(UIColor.purple, for: .normal)
        tsLbl.text = "Text & Speech (I)"
        currentGoal = 1

        recordView.isHidden = true

        requestSpeechAuthorization()
    }

    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "recordingContainer" {
            RV = segue.destination as? RecordView
        }
        if segue.identifier == "toHow" {

            let destination = segue.destination as? HowViewController
            destination?.goalID = goalID

        }
    }

    func resetButtons(){
        b1.setTitleColor(UIColor.black, for: .normal)
        b2.setTitleColor(UIColor.black, for: .normal)
        b3.setTitleColor(UIColor.black, for: .normal)
        b4.setTitleColor(UIColor.black, for: .normal)
        b5.setTitleColor(UIColor.black, for: .normal)
    }

    func showMsg(title:String="",message:String=""){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    func sendAlert(message: String) {
        let alert = UIAlertController(title: "Speech Recognizer Error", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    func readValues(){

        recordList.removeAll()
        recordTable.reloadData()

        let queryString = "SELECT * FROM WhyRecords WHERE goal_id = \(currentGoal!) AND parent_goal_id = \(goalID!)"

        var stmt:OpaquePointer?

        if sqlite3_prepare(db, queryString, -1, &stmt, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error preparing : \(errmsg)")
            return
        }

        while(sqlite3_step(stmt) == SQLITE_ROW){
            let id = sqlite3_column_int(stmt, 0)
            // parent_goal_id , 1
            // goal_id , 2
            let transcription = String(cString: sqlite3_column_text(stmt, 3))
            let file = String(cString: sqlite3_column_text(stmt, 4))
            let recdate = String(cString: sqlite3_column_text(stmt, 5))


            recordList.append(RecordList(id: Int(id), datetime: recdate, file: file, transcription: transcription))

            print("record \(id)")
        }

        recordList.reverse()

        recordTable.reloadData()

        recordTable.beginUpdates()

        recordTable.setContentOffset(.zero, animated: true)

        recordTable.endUpdates()

    }

    func onHideRecording(){

        recordView.isHidden = true

        finishRecording(success: true)

        cancelRecording()

        talkBack()

        readValues()
    }

    @IBAction func onRecord(_ sender: Any) {
        recordView.isHidden = false

        RV.updateLabel(txt: "")

        startRecording()

        recordAndRecognizeSpeech()

        isRecording = true
    }

    @IBAction func onExplanation(_ sender: Any) {
    }

    @IBAction func onVideo(_ sender: Any) {
    }

    @IBAction func onSettings(_ sender: Any) {
    }

    @IBAction func onHowTo(_ sender: Any) {

        performSegue(withIdentifier: "toHow", sender: self)
    }


    @IBAction func onB1(_ sender: Any) {
        resetButtons()
        b1.setTitleColor(UIColor.purple, for: .normal)
        tsLbl.text = "Text & Speech (I)"
        currentGoal = 1

        readValues()
    }
    @IBAction func onB2(_ sender: Any) {
        resetButtons()
        b2.setTitleColor(UIColor.purple, for: .normal)
        tsLbl.text = "Text & Speech (II)"
        currentGoal = 2

        readValues()
    }
    @IBAction func onB3(_ sender: Any) {
        resetButtons()
        b3.setTitleColor(UIColor.purple, for: .normal)
        tsLbl.text = "Text & Speech (III)"
        currentGoal = 3

        readValues()
    }
    @IBAction func onB4(_ sender: Any) {
        resetButtons()
        b4.setTitleColor(UIColor.purple, for: .normal)
        tsLbl.text = "Text & Speech (IV)"
        currentGoal = 4

        readValues()
    }
    @IBAction func onB5(_ sender: Any) {
        resetButtons()
        b5.setTitleColor(UIColor.purple, for: .normal)
        tsLbl.text = "Text & Speech (V)"
        currentGoal = 5

        readValues()
    }


    @IBAction func onBackToGoals(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }


}

extension WhyViewController: AVAudioRecorderDelegate, SFSpeechRecognizerDelegate, AVAudioPlayerDelegate{
    
    func requestSpeechAuthorization() {
        SFSpeechRecognizer.requestAuthorization { authStatus in
            OperationQueue.main.addOperation {
                switch authStatus {
                case .authorized:
                    self.recordButton.isEnabled = true
                    self.doRecording()
                case .denied:
                    self.recordButton.isEnabled = false
                    self.sendAlert(message: "User denied access to speech recognition")
                //self.textLbl.text = "User denied access to speech recognition"
                case .restricted:
                    self.recordButton.isEnabled = false
                    self.sendAlert(message: "Speech recognition is restricted on this device")
                //self.textLbl.text = "Speech recognition restricted on this device"
                case .notDetermined:
                    self.recordButton.isEnabled = false
                    self.sendAlert(message: "Speech recognition not yet authorized")
                }
            }
        }
    }

    func startRecording() {

        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]

        do {
            let uuid = UUID().uuidString.lowercased()
            audioFilename = getDocumentsDirectory().appendingPathComponent(uuid+".m4a")
            audioName = uuid+".m4a"

            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder.delegate = self
            audioRecorder.record()

        } catch {
            finishRecording(success: false)
        }
    }

    func doRecording(){

        recordingSession = AVAudioSession.sharedInstance()

        do {
            try recordingSession.setCategory(AVAudioSession.Category.playAndRecord, mode: AVAudioSession.Mode.default)
            try recordingSession.setActive(true)
        } catch {
            // failed to record!
            print ("failed to record !")
        }

        //the database file
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            .appendingPathComponent("RecDatabase.sqlite")

        //opening the database
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("error opening database")
        }

        //creating table
        if sqlite3_exec(db, "CREATE TABLE IF NOT EXISTS WhyRecords (id INTEGER PRIMARY KEY AUTOINCREMENT, parent_goal_id INTEGER, goal_id INTEGER, transcription TEXT, file TEXT, recdate TEXT)", nil, nil, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error creating table: \(errmsg)")
        }

        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("error opening database")
        }else{

            readValues()
        }

        print(fileURL)
    }

    func cancelRecording()
    {
        audioEngine.stop()

        audioEngine.inputNode.removeTap(onBus: 0)

        recognitionTask?.cancel()

        recognitionTask = nil

        isRecording = false

    }

    func talkBack(){
        let utterance = AVSpeechUtterance(string: txtLblString!)
        utterance.voice = AVSpeechSynthesisVoice()

        let synth = AVSpeechSynthesizer()
        synth.speak(utterance)
    }

    func finishRecording(success: Bool) {
        audioRecorder.stop()
        audioRecorder = nil

        if success {
            print("recorded")

            let todaysDate:Date = Date()
            let dateFormatter:DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let todayString:String = dateFormatter.string(from: todaysDate)

            let txt = txtLblString?.replacingOccurrences(of: "'", with: "`")
            let queryString = "INSERT INTO WhyRecords (parent_goal_id,goal_id, transcription, file, recdate) VALUES (\(goalID!),\(currentGoal!),'\(txt!)','\(audioName!)','\(todayString)')"

            print(queryString)

            if sqlite3_exec(db, queryString, nil, nil, nil) != SQLITE_OK {
                let errmsg = String(cString: sqlite3_errmsg(db)!)
                print("error: \(errmsg)")
            }

        } else {
            print("record failed")
        }
    }

    func recordAndRecognizeSpeech(){
        let node = audioEngine.inputNode
        let recordingFormat = node.outputFormat(forBus: 0)
        node.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { buffer, _ in
            self.request.append(buffer)

        }
        audioEngine.prepare()
        do {
            try audioEngine.start()
        } catch {
            self.sendAlert(message: "There has been an audio engine error.")
            return print(error)
        }
        guard let myRecognizer = SFSpeechRecognizer() else {
            self.sendAlert(message: "Speech recognition is not supported for your current locale.")
            return
        }
        if !myRecognizer.isAvailable {
            self.sendAlert(message: "Speech recognition is not currently available. Check back at a later time.")
            // Recognizer is not available right now
            return
        }
        recognitionTask = speechRecognizer?.recognitionTask(with: request, resultHandler: { (result, error) in
            if result != nil { // check to see if result is empty (i.e. no speech found)
                if let result = result {
                    let bestString = result.bestTranscription.formattedString
                    self.txtLblString = bestString

                    self.RV.updateLabel(txt: bestString)

                    //var lastString: String = ""
                    //for segment in result.bestTranscription.segments {
                    //let indexTo = bestString.index(bestString.startIndex, offsetBy: segment.substringRange.location)
                    //lastString = bestString.substring(from: indexTo)
                    //}

                    // here check for what is actually saying (recognize)

                } else if let error = error {
                    self.sendAlert(message: "There has been a speech recognition error")
                    print(error)
                }
            }

        })

    }

    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }

    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("audioPlayerDidFinishPlaying",flag)
    }
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print("Audio Play Decode Error",error.debugDescription)
    }

    func audioRecorderDidFinishRecording(recorder: AVAudioRecorder!, successfully flag: Bool) {
        print("audioRecorderDidFinishRecording",flag)
    }

    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
        print("Audio Record Encode Error",error.debugDescription)
    }
}

extension WhyViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recordList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! GoalCell

        cell.goalLbl.text = recordList[indexPath.row].transcription

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //    performSegue(withIdentifier: "toWhy", sender: self)
    }

}
