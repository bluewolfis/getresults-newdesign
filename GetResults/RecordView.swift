//
//  RecordView.swift
//  GetResults
//
//  Created by Alex Balan on 28/11/2018.
//  Copyright © 2018 Alex Balan. All rights reserved.
//

import UIKit

class RecordView: UIViewController {

    @IBOutlet weak var txtLbl: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    func updateLabel(txt:String){
        txtLbl.text = txt
    }

    @IBAction func onClose(_ sender: Any) {

        if let parentview = parent as? MainViewController{
            parentview.onHideRecording()
            return
        }

        if let parentview = parent as? WhyViewController{
            parentview.onHideRecording()
            return
        }

        if let parentview = parent as? HowViewController{
            parentview.onHideRecording()
            return
        }

        print("this should never appear")

    }


}
