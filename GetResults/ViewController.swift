//
//  ViewController.swift
//  GetResults
//
//  Created by Alex Balan on 22/11/2018.
//  Copyright © 2018 Alex Balan. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit
import GoogleSignIn

class ViewController: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate {

    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        print("Google Sing In didSignInForUser")
        if let error = error {
            print(error.localizedDescription)
            self.showMsg(title:"Error",message: "Error, "+error.localizedDescription)

            return
        }

        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: (authentication.idToken)!, accessToken: (authentication.accessToken)!)
        // When user is signed in

        Auth.auth().signInAndRetrieveData(with: credential) { (result, error) in

            if(error == nil){
                //user created or logged in successfully, move to next screen

                self.performSegue(withIdentifier: "makelogin", sender: self)

            }else{
                //error
                print(error.debugDescription)
                self.showMsg(title:"Error",message: "Error, "+(error?.localizedDescription)!)
            }

        }
    }

    func showMsg(title:String="",message:String=""){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func doLogin(_ sender: Any) {
        Auth.auth().signIn(withEmail: email.text!, password: password.text!) { (result, err) in

            if(err != nil){
                self.showMsg(title:"Error",message: err?.localizedDescription ?? "Unkown Error")
            }else{
                //print(result)
                self.performSegue(withIdentifier: "makelogin", sender: self)

            }

        }
    }

    @IBAction func doRegister(_ sender: Any) {
        Auth.auth().createUser(withEmail: email.text!, password: password.text!) { (result, err) in

            if(err != nil){
                self.showMsg(title:"Error",message: err?.localizedDescription ?? "Unkown Error")
            }else{
                //print(result)
                self.performSegue(withIdentifier: "makelogin", sender: self)

            }

        }
    }

    @IBAction func doFacebookLogin(_ sender: Any) {
        let loginManager = FBSDKLoginManager()

        loginManager.logIn(withReadPermissions: ["public_profile","email"], from: self) { (result, error) in
            if(error == nil){

                if(result?.isCancelled)!{
                    //user canceled request
                }else{
                    //login with facebook successfull
                    let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)

                    Auth.auth().signInAndRetrieveData(with: credential, completion: { (user, error) in
                        if(error == nil){
                            //user created or logged in successfully, move to next screen

                            self.performSegue(withIdentifier: "makelogin", sender: self)

                        }else{
                            //error
                            print(error.debugDescription)
                            self.showMsg(title:"Error",message: "Error, "+(error?.localizedDescription)!)
                        }
                    })

                }
            }else{
                print(error.debugDescription)
                self.showMsg(title:"Error",message: "Error, "+(error?.localizedDescription)!)
            }
        }
    }
    @IBAction func doGoogleLogin(_ sender: Any) {
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signIn()
    }

}

