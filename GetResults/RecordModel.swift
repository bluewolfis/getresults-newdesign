//
//  RecordList.swift
//  GetResults
//
//  Created by Alex Balan on 28/11/2018.
//  Copyright © 2018 Alex Balan. All rights reserved.
//

import Foundation

class RecordList {

    var id: Int
    var datetime: String?
    var file: String?
    var transcription: String?

    init(id: Int, datetime: String?, file:String?, transcription:String?){
        self.id = id
        self.datetime = datetime
        self.file = file
        self.transcription = transcription
    }
}
